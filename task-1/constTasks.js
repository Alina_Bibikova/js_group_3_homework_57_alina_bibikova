const tasks = [
    {id: 234, title: 'Create user registration API', timeSpent: 4, category: 'Backend', type: 'task'},
    {id: 235, title: 'Create user registration UI', timeSpent: 8, category: 'Frontend', type: 'task'},
    {id: 237, title: 'User sign-in via Google UI', timeSpent: 3.5, category: 'Frontend', type: 'task'},
    {id: 238, title: 'User sign-in via Google API', timeSpent: 5, category: 'Backend', type: 'task'},
    {id: 241, title: 'Fix account linking', timeSpent: 5, category: 'Backend', type: 'bug'},
    {id: 250, title: 'Fix wrong time created on new record', timeSpent: 1, category: 'Backend', type: 'bug'},
    {id: 262, title: 'Fix sign-in failed messages', timeSpent: 2, category: 'Frontend', type: 'bug'},
];

const result = tasks.reduce((accumulator, currentVal) => {
    if (currentVal.category === "Frontend") accumulator += currentVal.timeSpent;
    return accumulator
}, 0);
console.log(result);

const totalTasks = tasks.reduce((accumulator, currentVal) => {
    if (currentVal.category === "Frontend") accumulator.Frontend++;
    if (currentVal.category === "Backend") accumulator.Backend++;
    return accumulator
}, {Frontend: 0, Backend: 0});
console.log(totalTasks);


const UITasks = tasks.filter(elem => elem.title.includes("UI")).length;
console.log(UITasks);

const totalBug = tasks.reduce((accumulator, currentVal) => {
    if (currentVal.type === "bug") accumulator += currentVal.timeSpent;
    return accumulator;
}, 0);
console.log(totalBug);

const titleTasks = tasks.map( element => {
    if (element.timeSpent >= 4){
        return {title: element.title, category:element.category}
    }
}).filter(elem => elem !== undefined);
console.log(titleTasks);

