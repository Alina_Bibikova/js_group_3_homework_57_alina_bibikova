import React, {Component, Fragment} from 'react';
import ContentAdded from '../../components/ContentAdded/ContentAdded';
import ContentAll from '../../components/ContentAll/ContentAll'

class GeneralContent extends Component {

    state = {
        itemName: '',
        cost: '',
        newItem: [],
        totalPrice: 0
    };

    changeInput = event => {
        this.setState({itemName: event.target.value})
    };

    changeCost = (event) => {
        if (!isNaN(this.state.cost)) {
            this.setState({
                cost: event.target.value
            })
        } else this.setState({cost: ''})
    };

    addedClick = () => {
        if(this.state.itemName !== '' && this.state.cost !== '') {
            const items = [...this.state.newItem];
            const oldPrice = this.state.totalPrice;
            const newPrice = oldPrice + parseInt(this.state.cost);

            const newItem = {
                text: this.state.itemName,
                cost: this.state.cost
            };
            items.push(newItem);

            this.setState({
                newItem: items,
                itemName: '',
                cost: '',
                totalPrice: newPrice
            });
        } else {
            alert('Please fill all fields!');

        }
    };

    removeClick = (text, cost) => {
        let itemArray = [...this.state.newItem];

        const index = itemArray.findIndex((item) => item.text === text);
        itemArray.splice(index, 1);
        const oldPrice = this.state.totalPrice;
        const newPrice = oldPrice - parseInt(cost);

        this.setState({newItem: itemArray,
            totalPrice: newPrice
        })
    };

    render() {
        return (
            <Fragment>
                <ContentAdded
                addedClick={this.addedClick}
                itemName={this.state.itemName}
                cost={this.state.cost}
                changed={(event) => this.changeInput(event)}
                changeCost={(event) => this.changeCost(event)}
                >
                    {this.state.newItem.map((task, key) => {
                        return <ContentAll key={key} contentAll={task} id={key} removeItem={() => this.removeClick(task.text , task.cost)}
                        />
                        })
                    }
                    <p>Total price: <strong>{this.state.totalPrice} KGS</strong></p>
                </ContentAdded>
            </Fragment>
        )
    }
}

export default GeneralContent;
