import React from 'react';
import './ContentAll.css';

const ContentAll = props => {
    return (
        <div className='contentAll'>
            <span>{props.contentAll.text}</span>
            <span> : {props.contentAll.cost} KGS</span>
            <button className='removeItem' onClick={props.removeItem}>x</button>
        </div>
    )
};

export default ContentAll;