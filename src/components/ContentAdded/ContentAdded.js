import React from 'react';
import './ContentAdded.css'

const ContentAdded = props => (
    <div className="ContentAdded">
        <input className='itemName'
               type="ItemName"
               value={props.itemName}
               onChange={props.changed}
               placeholder='Item name'
        />
        <input className='cost'
               type="cost"
               value={props.cost}
               onChange={props.changeCost}
               placeholder='Cost'
        />
        <span>KGS </span>
        <button className='added' onClick={props.addedClick}>Added</button>
        <div>
            {props.children}
        </div>
    </div>
);

export default ContentAdded;